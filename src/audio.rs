const EXTS: [&'static str; 3] = [//valid music file extensions
	"mp3", 
	"flac", 
	"wav"
];

extern crate rfmod;
extern crate walkdir;
use std::{
	vec,
	io,
	fs::{self, DirEntry},
	path::Path,
	path::PathBuf,
	thread::sleep,
	time::Duration
};
use walkdir::WalkDir;

pub struct Song {//struct to represent songs
	sound: rfmod::Sound,
	pos: u64
}

impl Song {
	pub fn new(name: &str, sys: &rfmod::Sys) -> Song {
		Song {
			sound: sys.create_sound(name, None, None).unwrap(),
			pos: 0u64
		}
	}
	pub fn play(&self) {
		match self.sound.play_to_the_end() {
			rfmod::Status::Ok => {},
			err => {panic!("Failed to play sound: {:?}", err)},
		};
	}
}

pub struct Queue {//struct to represent queued songs
	songs: Vec<Song>,
	cur_song: usize,
	paused: bool
}

impl Queue {
	pub fn new() -> Queue {
		Queue {
			songs: Vec::<Song>::new(),
			cur_song: 0usize,
			paused: true
		}
	}
	fn add_song(&mut self, song: Song) {
		self.songs.push(song);
	}
	pub fn del_song(&mut self, song: usize) {
		self.songs.remove(song);
	}
	pub fn get_song(&self) -> &Song {
		return &self.songs[self.cur_song];
	}
	pub fn is_paused(&self) -> bool {
		return self.paused;
	}
	pub fn pause(&mut self) {
		self.paused = true;
	}
	pub fn play(&mut self) {
		self.paused = false;
	}
	pub fn toggle(&mut self) {
		self.paused = !self.paused;
	}
	//create and add songs to queue from a Vec<PathBuf>
	pub fn gen_queue(&mut self, sys: &rfmod::Sys, paths: Vec<PathBuf>) {
		for path in paths {
			let path = path.to_str().unwrap();
			let newsong = Song::new(path, sys);
			self.songs.push(newsong);
		}
	}
	//update queue
	pub fn update(&mut self) {
		self.songs[self.cur_song].play();
		self.cur_song+=1;
	}
}

pub fn setup_audio(libPath: &str) -> (rfmod::Sys, Vec<PathBuf>) {//setup and init FMOD system
	let fmod = match rfmod::Sys::new() {
		Ok(f) => f,
		Err(e) => {panic!("Failed to create FMOD system: {:?}", e)},
	};

	match fmod.init() {
		rfmod::Status::Ok => {},
		e => {panic!("Failed to init FMOD system: {:?}", e)},
	};

	let queue = get_files_in_dir(libPath);

	return (fmod, queue);
}

//wrapper function for directory traversal
fn get_files_in_dir(dir: &str) -> Vec<PathBuf> {
	let mut files = Vec::new();
	for entry in WalkDir::new(dir) {
		let entry = entry.unwrap().into_path();
		if is_music(&entry) == true {
			files.push(entry);
		}
	}
	return files;
}

fn is_music(path: &Path) -> bool {//check if file is a music file
	let iext = match path.extension() {
		Some(s) => s,
		None => {return false;},
	};
	for ext in EXTS.iter() {
		if ext == &iext.to_str().unwrap() {
			return true;
		}
	}
	return false;
}

fn play_file(path: &str, fmod: rfmod::Sys) {
	let sound = match fmod.create_sound(path, None, None) {
		Ok(s) => s,
		Err(e) => {panic!("Failed to create sound from file: {:?}", e)},
	};
	match sound.play_to_the_end() {
		rfmod::Status::Ok => {},
		err => {panic!("Failed to play sound: {:?}", err)},
	};
}
