mod library;
mod tui;
mod audio;

fn main() {
	let (fmod, library) = audio::setup_audio("/home/taco/Localmus");
	let mut queue = audio::Queue::new();
	queue.gen_queue(&fmod, library);//load library into queue
	queue.update();

	tui::create_tui();
}
