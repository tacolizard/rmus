extern crate cursive;

use cursive::Cursive;
use cursive::views::{
	Button,
	Dialog,
	DummyView,
	EditView,
	LinearLayout,
	SelectView
};
use cursive::traits::*;

pub fn create_tui() {
	let mut siv = Cursive::default();//create Cursive object

	let select = SelectView::<String>::new()
		.on_submit(on_submit)
		.with_id("select")
		.fixed_size((10, 5));


	siv.add_global_callback('q', |s| s.quit());
	//siv.run();
}

fn on_submit(s: &mut Cursive, name: &String) {
	s.pop_layer();
	s.add_layer(Dialog::text(format!("test"))
		.title(format!("test list"))
		.button("Quit", Cursive::quit));
}
